# fe-ux-library-bootstrap

> My delightful Nuxt.js project

## TODO LIST: 

1. https://github.com/SafeStudio/nuxt-bootstrap-boilerplate/

2. Imitate this boy
https://bulmatemplates.github.io/bulma-templates/

[] - Build Bootstrap cards


PAGES: 
[] - Bootstrap Basics
[] - Bootstrap Fancy
[] - Website 1 

COMPONENTS: 
Defaults: 
[] - Build Bootstrap defaults page
[] - Bootstrap Alerts, Badges, Carousel, Dropdown




3. Random name generator


Todo list: 

1. Pull down 6 sites where we use Bootstrap heavily and steal their design.

2. Create a second box where it breaks it into direct components. 
So it's [pages | components]

3. Codepen integration?

4. Add the codeblock thing so you can copy/paste code. 


## Build Setup

```bash
# 1. install dependencies
$ npm install

# 2. serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
